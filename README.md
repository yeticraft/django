# Django In A Box

## Purpose

With this basic docker image, you can quickly and easily generate a new, independant, custom, dockerized, Django application project within minutes.

### What this Tutorial-Project is _not_

It is not the intention of this documentation (or project) to be a complete Docker implementation guide - nor a complete Django Guide. Furthermore, since most of the commands and tools are Linux based, we will assume a basic knowledge of Linux OS and networking in general and not be delving too deeply into basic Linux knowledge.

It is important to note that while this tool can (and should) be used to create the _basis_ of a "production ready" product, it should not be expected to result in the _entirety_ of one.  Rather, the intent is to demonstrate some of the complexities of creating a Docker Image of a Django project, and to ease the generation of a clean foundation - with close attention to simplicity, reusability and allowing continued flexibility of development afterwards.  

In other words: Aditional development and coding will be necessary before you would want to take your project live - and we will have notes on where to go next for that end.

### What this Tutorial-Project _is_

Django is an extremely dynamic and powerful tool, so it is difficult to simply create a 'one size fits all' or 'pre-built' container image that has everything that everyone needs for every purpose.  However, this project aims to assist new developers with easily and quickly generating a foundation for their *own* image which will not limit creativity or preselect any components more than necessary - preserving the power of Django's versatility (and retaining total developer customization autonomy) as much as possible at every turn.

### A Note About Default Component Support: 

Because DjangoCMS is one of my personal favorite components for Django, it is the first variant that I chose to add support for within the creation image. With that said, however, support for other starting options (such as Wagtail, FeinCMS, ViewFlo, and more) is being created as quickly as possible.  Please _do_ feel free to [fork](https://bitbucket.org/yeticraft/django/fork) this repository and create pull requests with more options.  We enthusiastically welcome collaboration.  :)

## Documentation and Code

Please see the [Wiki on BitBucket](https://bitbucket.org/yeticraft/django/wiki/Home) for the detailed instructions, help, and access to the code.  Please feel free to fork and build new options for this tool.  We encourage pull-requests!!
