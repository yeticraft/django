###################################
# Development settings - these should editied to taste
# after site creation.  These are just here as a guideline
# and to make starting your project a little bit easier.

DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS += [
    # 'djangocms_admin_style',
    # 'django.contrib.sites',
    # 'django.contrib.sitemaps',
    'django.contrib.admindocs',
    # 'registration',
    # 'django_extensions',
    'debug_toolbar',
]

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
] + MIDDLEWARE

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# DebugToolbar
# https://django-debug-toolbar.readthedocs.io/en/
# To use this from inside a running container you need to add the containers gateway from the following command:
#   docker inspect mysite | grep -e '"Gateway"'
INTERNAL_IPS = ['127.0.0.1', '::1', ]

